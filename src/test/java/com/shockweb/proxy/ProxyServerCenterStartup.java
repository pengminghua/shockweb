/*
 *
 * Copyright (c) 2017, baiwang.com. All rights reserved.
 * All rights reserved.
 * Author: zhengb
 * Created: 2017/01/11
 * Description:
 *
 */

package com.shockweb.proxy;

/**
 * 独立启动
 * 
 * @author 彭明华
 * 2017年12月14日 创建
 */
public class ProxyServerCenterStartup {
	
    public static void main(String[] args) throws Exception {
    	ProxyServer.main(new String[]{"127.0.0.1:2000,127.0.0.1:2002", "proxy"});
    }
}
