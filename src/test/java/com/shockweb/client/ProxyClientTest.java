package com.shockweb.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.shockweb.common.context.ContextParam;
import com.shockweb.bridge.OperationDefine;
import com.shockweb.bridge.ServerInfo;
import com.shockweb.bridge.ServiceRequest;
import com.shockweb.client.Client;
import com.shockweb.client.exception.ClientException;
import com.shockweb.client.impl.ServiceClient;
import com.shockweb.register.RegisterServerNettyHandler;




/**
 * Unit test for simple App.
 */
public class ProxyClientTest 
{
	@Test
    public  void test(){
		ServiceClient client = null;
		try{
			client = new ServiceClient();
			client.connect("127.0.0.1:5000");
			ServiceRequest request = new ServiceRequest();
			request.setSpaceName("default");
			request.setService("TestService");
			request.setContext(new ContextParam(UUID.randomUUID().toString()));
			request.setParameterTypes(new Class[]{String.class,Integer.class});
			request.setParams(new Object[]{"123",new Integer(1)});
			request.setMethod("service");
			System.out.println(client.rpcService(request));
		}catch(Throwable e){
			e.printStackTrace();
		}finally{
			if(client!=null){
				client.close();
			}
		}
		

    }


}
