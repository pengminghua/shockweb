package com.shockweb.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.shockweb.bridge.OperationDefine;
import com.shockweb.bridge.ServerInfo;
import com.shockweb.client.Client;
import com.shockweb.client.exception.ClientException;
import com.shockweb.register.RegisterServerNettyHandler;




/**
 * 多client压测
 */
public class MicroCenterClientsTest implements Runnable
{
	static List<Client> clients = new ArrayList<Client>();
	
	List<Thread> lists = new ArrayList<Thread>();
	@Test
    public  void test(){
		try{
			Client client = new Client();
			client.connect("127.0.0.1:3000");
			clients.add(client);
			client = new Client();
			client.connect("127.0.0.1:3001");
			clients.add(client);
			client = new Client();
			client.connect("127.0.0.1:3000");
			clients.add(client);
			client = new Client();
			client.connect("127.0.0.1:3001");
			clients.add(client);
			for(int i=0;i<4;i++){
				lists.add(new Thread(new MicroCenterClientsTest()));
				lists.get(i).start();
			}
			boolean stop = false;
			while(!stop){
				stop = true;
				for(Thread t:lists){
					if(t.isAlive()){
						stop = false;
						break;
					}
				}
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}catch(Throwable e){
			e.printStackTrace();
		}finally{
			if(clients!=null){
				for(Client c:clients){
					c.close();
				}
			}
		}
		

    }

	private static int i=0;
	public void run(){
		try {
			int index = i;
			i++;
			index = index % clients.size();
			Client client = clients.get(index);
			
			ServerInfo info = new ServerInfo();
			long time = System.currentTimeMillis();
			System.out.println("start:" + time);
			long t = 0;
			long t2 = 0;
			long t3 = 0;
			ServerInfo rtn = null;
			for(int i=0;i<10000;i++){
				try{
					long time1 = System.currentTimeMillis();
					info.setCalled(time1);
					rtn = (ServerInfo)client.send(OperationDefine.REQ_PUT_SERVICES,info,null);
					t2 = t2 + System.currentTimeMillis() - rtn.getCalled();
					t3 = t3 + rtn.getCalled() - time1;
					t = t + System.currentTimeMillis() - time1;
				}catch(Exception e){
					e.printStackTrace();
				}
				if(i%1000==0){
					System.out.println("thread:" + Thread.currentThread() + i + "=" + (System.currentTimeMillis()-time));
				}
			}
			System.out.println("end:" + Thread.currentThread() + (System.currentTimeMillis()-time) + ",整体时间：" + t + 
					",返回时间：" + t2 + ",调用时间：" + t3);		
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}finally{

		}


	}

}
