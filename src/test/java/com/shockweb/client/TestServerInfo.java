package com.shockweb.client;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.shockweb.bridge.HostInfo;
import com.shockweb.bridge.OperationDefine;
import com.shockweb.bridge.ServerInfo;
import com.shockweb.client.Client;
import com.shockweb.client.exception.ClientException;




/**
 * Unit test for simple App.
 */
public class TestServerInfo{


    /**
     * @return the suite of tests being tested
     * @throws RpcException 
     */
	
	static Client client = new Client();

	@Test
    public  void test(){
		try{
			long time = System.currentTimeMillis();
			client.connect("127.0.0.1:3001");
			System.out.println(System.currentTimeMillis()-time);
			ServerInfo server = new ServerInfo();
			server.setCalled(123);
			server.setHost("127.0.0.1:3000");
			List<String> list = new ArrayList<String>();
			list.add("serviceTestNames");
			server.setServiceNames(list);
			server.setSpaceName("spaceName1");
			System.out.println("start:" + time);
			Object rtn = client.send(OperationDefine.REQ_PUT_SERVICES,server,null);
			HostInfo host = new HostInfo();
			host.setCalled(345);
			host.setHost("127.0.0.1:3000");
			for(int i=0;i<100;i++){
				try{
					rtn = client.send(OperationDefine.REQ_PUT_HOSTS,host,null);
				}catch(Exception e){
					e.printStackTrace();
				}
				Thread.sleep(1000);
			}

			System.out.println("end:" + Thread.currentThread() + (System.currentTimeMillis()-time));
		}catch(Throwable e){
			e.printStackTrace();
		}finally{
			if(client!=null){
				client.close();
			}
		}
		

    }


}
