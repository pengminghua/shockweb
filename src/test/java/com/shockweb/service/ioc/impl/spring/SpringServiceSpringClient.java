package com.shockweb.service.ioc.impl.spring;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.shockweb.rpc.spring.ScanClass11;

public class SpringServiceSpringClient implements Runnable{
	
	ClassPathXmlApplicationContext ctx = null;
	
	public SpringServiceSpringClient(ClassPathXmlApplicationContext ctx){
		this.ctx = ctx;
	}
	public void run(){
		long time = System.currentTimeMillis();
		ScanClass11 scanClass11 = (ScanClass11)ctx.getBean("ScanClass11");
		System.out.println("start:" + time);
		for(int i=0;i<10000;i++){
			try{
				scanClass11.print();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		System.out.println("end:" + Thread.currentThread() + (System.currentTimeMillis()-time));
	}
}