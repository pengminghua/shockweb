package com.shockweb.service.ioc.impl.spring;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.shockweb.common.context.ContextManager;
import com.shockweb.bridge.ServiceRequest;
import com.shockweb.rpc.RpcManager;
import com.shockweb.rpc.spring.ScanClass11;
import com.shockweb.rpc.spring.ScanClass12;
public class ScanTest {
	


	 public static void main(String[] args) throws Exception {

		 ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext2.xml");
		 RpcManager.init("classpath:config.properties");
		 System.out.println(RpcManager.getClientManager().getRegisterClient().queryServices());

		 ScanClass11 scanClass11 = (ScanClass11)ctx.getBean("ScanClass11");
		 long t = System.currentTimeMillis();
		 
		 scanClass11.test(null);
		 for(int i=0;i<10000;i++){
			 scanClass11.print();
		 }
		 System.out.println("Spring Service+Spring Client:" + (System.currentTimeMillis()-t));
		 ServiceRequest request = new ServiceRequest();
		 request.setContext(ContextManager.getContextParam());
		 request.setMethod("print");
		 request.setService("ScanClass11");
		 t = System.currentTimeMillis();
		 for(int i=0;i<10000;i++){
			 RpcManager.getClientManager().getClient("default", "ScanClass11").rpcService(request);
		 }
		 System.out.println("Spring Service+rpc Client:" + (System.currentTimeMillis()-t));
		 ScanClass12 scanClass12 = (ScanClass12)ctx.getBean("ScanClass12");
		 t = System.currentTimeMillis();
		 for(int i=0;i<10000;i++){
			 scanClass12.print();
		 }
		 System.out.println("default Service+Spring Client:" + (System.currentTimeMillis()-t));
		 request.setService("ScanClass12");
		 t = System.currentTimeMillis();
		 for(int i=0;i<10000;i++){
			 RpcManager.getClientManager().getClient("default", "ScanClass12").rpcService(request);
		 }
		 System.out.println("default Service+rpc Client:" + (System.currentTimeMillis()-t));
		 
		 t = System.currentTimeMillis();
		 for(int i=0;i<10000;i++){
			 RpcManager.getClientManager().getClient("default", "ScanClass12").rpcService("ScanClass12", "test", null);
		 }
		 System.out.println("byte[] Service+byte[] Client:" + (System.currentTimeMillis()-t));
		 System.out.println(RpcManager.getClientManager().getRegisterClient().queryServices());
		 RpcManager.close();
	 }
	 
	 

}