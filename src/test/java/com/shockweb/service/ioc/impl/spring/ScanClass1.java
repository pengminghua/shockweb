package com.shockweb.service.ioc.impl.spring;

import org.springframework.beans.factory.annotation.Autowired;

import com.shockweb.rpc.spring.ScanClass11;
import com.shockweb.service.ioc.impl.spring.ShockWebService;

@ShockWebService(value="ScanClass11")
public class ScanClass1 {
	

	public void print() {
//		System.out.println("scanClass1");
	}
	
	public byte[] test(byte[] data){
		return data;
	}

}