package com.shockweb.rpc.spring;

import com.shockweb.rpc.spring.ShockWebRemote;

@ShockWebRemote(value="ScanClass12",spaceName="default")
public interface ScanClass12 extends ScanInter5{
	public void print();
	
	public byte[] test(byte[] data);
}