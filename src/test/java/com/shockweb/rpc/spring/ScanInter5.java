package com.shockweb.rpc.spring;

import java.util.Map;

import com.shockweb.rpc.spring.ShockWebRemote;

/**
 * 远程服务端接口
 * 
 * @author 彭明华
 * 2018年2月28日 创建
 */
@ShockWebRemote
public interface ScanInter5 {

	public void print();
}