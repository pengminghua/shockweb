/*
 *
 * Copyright (c) 2017, baiwang.com. All rights reserved.
 * All rights reserved.
 * Author: zhengb
 * Created: 2017/01/11
 * Description:
 *
 */

package com.shockweb.register;

import com.shockweb.common.sitelocal.LANAddress;
import com.shockweb.register.RegisterServer;
/**
 * 独立启动
 * 
 * @author 彭明华
 * 2017年12月14日 创建
 */
public class RegisterStartup {
	

    

    public static void main(String[] args) throws Exception {
       	RegisterServer.startCluster("127.0.0.1:3000",null);
    }
}
