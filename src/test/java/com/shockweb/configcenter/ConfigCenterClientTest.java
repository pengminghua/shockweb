package com.shockweb.configcenter;

import org.junit.Test;

import com.shockweb.client.impl.ConfigCenterClient;

/**
 * Unit test for simple App.
 */
public class ConfigCenterClientTest{
//	@Test
    public void testQueryConfigGroup() throws Exception {
    	ConfigCenterClient client1 = null;
    	ConfigCenterClient client2 = null;
    	try{
	    	client1 = new ConfigCenterClient();
	    	client2 = new ConfigCenterClient();
	    	client1.connect("127.0.0.1:2000");
	    	client2.connect("127.0.0.1:2002");
	    	client1.setSecretKey("ABCD");
	    	client2.setSecretKey("ABCD");
	    	System.out.println("127.0.0.1:2000=" + client1.queryConfigGroup());
	    	System.out.println("127.0.0.1:2002=" + client2.queryConfigGroup());
    	}catch(Exception e){
    		e.printStackTrace();
    	}finally{
    		if(client1!=null){
    			client1.close();
    		}
    	}
    }
	
	@Test
    public void testClear() throws Exception {
    	ConfigCenterClient client1 = null;
    	ConfigCenterClient client2 = null;
    	try{
	    	client1 = new ConfigCenterClient();
	    	client2 = new ConfigCenterClient();
	    	client1.connect("127.0.0.1:2000");
	    	client2.connect("127.0.0.1:2002");
	    	client1.setSecretKey("ABCD");
	    	client2.setSecretKey("ABCD");
	    	client1.clear();
	    	Thread.sleep(5000);
	    	System.out.println("127.0.0.1:2000=" + client1.queryConfigGroup());
	    	System.out.println("127.0.0.1:2002=" + client2.queryConfigGroup());
	    	client1.putConfig("group1", "test1", "value1");
	    	Thread.sleep(5000);
	    	System.out.println("127.0.0.1:2000=" + client1.queryConfigGroup());
	    	System.out.println("127.0.0.1:2002=" + client2.queryConfigGroup());
    	}catch(Exception e){
    		e.printStackTrace();
    	}finally{
    		if(client1!=null){
    			client1.close();
    		}
    	}
    }

}
