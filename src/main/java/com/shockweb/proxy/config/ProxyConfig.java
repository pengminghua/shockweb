package com.shockweb.proxy.config;

import com.shockweb.rpc.config.ClientConfig;

/**
 * 微服务服务器配置
 * 
 * @author 彭明华
 * 2018年1月3日 创建
 */
public class ProxyConfig extends ClientConfig{

	/**
	 * 配置中心地址
	 */
	private String configCenterUrls = null;
	
	/**
	 * 设置配置中心地址
	 * @param configCenterUrls
	 */
	public void setConfigCenterUrls(String configCenterUrls){
		this.configCenterUrls = configCenterUrls;
	}
	
	/**
	 * 配置中心地址
	 * @return
	 */
	public String getConfigCenterUrls(){
		return configCenterUrls;
	}
	
	/**
	 * 配置中心group
	 */
	private String configCenterGroup = null;
	
	/**
	 * 设置配置中心group
	 * @param configCenterGroup
	 */
	public void setConfigCenterGroup(String configCenterGroup){
		this.configCenterGroup = configCenterGroup;
	}
	
	/**
	 * 配置中心group
	 * @return
	 */
	public String getConfigCenterGroup(){
		return configCenterGroup;
	}


	/**
	 * 当前注册服务器的url
	 */
	private String hostUrl = null;
	
	/**
	 * 设置当前注册服务器的url
	 * @param hostUrl
	 */
	public void setHostUrl(String hostUrl){
		this.hostUrl = hostUrl;
	}
	
	/**
	 * 当前注册服务器的url
	 * @return
	 */
	public String getHostUrl(){
		return hostUrl;
	}
	

	
	/**
	 * 服务端每次检测netty客户端心跳间隔时间
	 */
	private int serverIdleStateTime = 1000*12;
	
	/**
	 * 设置服务端每次检测netty客户端心跳间隔时间
	 * @param serverIdleStateTime
	 */
	public void setServerIdleStateTime(int serverIdleStateTime){
		this.serverIdleStateTime = serverIdleStateTime;
	}
	
	/**
	 * 服务端每次检测netty客户端心跳间隔时间
	 * @return
	 */
	public int getServerIdleStateTime() {
		return serverIdleStateTime;
	}
	
	/**
	 * 客户端请求的超时时间
	 */
    private int serviceTimeOut = 5000;
    
	/**
	 * 设置客户端请求的超时时间
	 * @param clientTimeOut
	 */
	public void setServiceTimeOut(int serviceTimeOut){
		this.serviceTimeOut = serviceTimeOut;
	}
	
	/**
	 * 客户端请求的超时时间
	 * @return
	 */
	public int getServiceTimeOut() {
		return serviceTimeOut;
	}
}
