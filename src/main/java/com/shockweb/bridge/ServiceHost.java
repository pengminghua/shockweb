package com.shockweb.bridge;


/**
 * 提供服务的主机
 * 
 * @author 彭明华
 * 2017年12月12日 创建
 */
public class ServiceHost {


	/**
	 * 提供服务的主机地址
	 */
	private String host = null;
	/**
	 * 提供服务的主机地址
	 * @return
	 */
	public String getHost(){
		return host;
	}
	
	/**
	 * 设置提供服务的主机地址
	 * @param host
	 */
	public void setHost(String host){
		this.host = host;
	}
	
	/**
	 * 当前连接数
	 */
	private long called = 0;
	
	/**
	 * 当前连接数
	 * @return
	 */
	public long getCalled(){
		return called;
	}
	
	/**
	 * 设置当前连接数
	 * @param called
	 */
	public void setCalled(long called){
		this.called = called;
	}
	
	/**
	 * 更新时间
	 */
	private long updateTime = System.currentTimeMillis();
	/**
	 * 更新时间
	 * @return
	 */
	public long getUpdateTime(){
		return updateTime;
	}
	
	/**
	 * 设置更新时间
	 * @param updateTime
	 */
	public void setUpdateTime(long updateTime){
		this.updateTime = updateTime;
	}
	

	/**
	 * 同步线程的等待时间
	 */
	public int sleepTime = 0;
	/**
	 * 设置同步线程的等待时间
	 * @param sleepTime
	 */
	public void setSleepTime(int sleepTime){
		this.sleepTime = sleepTime;
	}
	
	/**
	 * 同步线程的等待时间
	 * @return
	 */
	public int getSleepTime(){
		return sleepTime;
	}
	
	/**
	 * 超时的调用次数
	 */
	public long timeOut = 0;
	/**
	 * 设置超时的调用次数
	 * @param timeOut
	 */
	public void setTimeOut(long timeOut){
		this.timeOut = timeOut;
	}
	
	/**
	 * 超时的调用次数
	 * @return
	 */
	public long getTimeOut(){
		return timeOut;
	}

	/**
	 * 正在执行的服务数量
	 */
	public long doing = 0;
	/**
	 * 设置正在执行的服务数量
	 * @param doing
	 */
	public void setDoing(long doing){
		this.doing = doing;
	}
	
	/**
	 * 正在执行的服务数量
	 * @return
	 */
	public long getDoing(){
		return doing;
	}

	/**
	 * 正在执行的服务数量阈值
	 */
	public long threshold = 0;
	/**
	 * 设置正在执行的服务数量阈值
	 * @param threshold
	 */
	public void setThreshold(long threshold){
		this.threshold = threshold;
	}
	
	/**
	 * 正在执行的服务数量阈值
	 * @return
	 */
	public long getThreshold(){
		return threshold;
	}
	
	/**
	 * @see Object#toString()
	 */
	public String toString(){
		StringBuilder sb = new StringBuilder("host:");
		sb.append(host).append(",called:").append(called);
		sb.append(",updateTime:").append(updateTime).append(",sleepTime:").append(sleepTime).append(",timeOut:").append(timeOut).
		append(",doing:").append(doing).append(",threshold:").append(threshold);;
		return sb.toString();
	}
}
