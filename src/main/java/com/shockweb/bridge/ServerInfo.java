package com.shockweb.bridge;

import java.util.List;

/**
 * 服务给注册中心发送的同步服务和服务器数据
 * 
 * @author 彭明华
 * 2017年12月13日 创建
 */
public class ServerInfo extends HostInfo{
	
	/**
	 * 服务空间名
	 */
	private String spaceName = null;
	
	/**
	 * 服务空间名
	 * @return
	 */
	public String getSpaceName(){
		return spaceName;
	}
	
	/**
	 * 设置服务空间名
	 * @param spaceName
	 */
	public void setSpaceName(String spaceName){
		this.spaceName = spaceName;
	}
	
	/**
	 * 提供的服务名
	 */
	private List<String> serviceNames = null;
	
	/**
	 * 提供的服务名
	 * @return
	 */
	public List<String> getServiceNames(){
		return serviceNames;
	}
	
	/**
	 * 设置提供的服务名
	 * @param serviceNames
	 */
	public void setServiceNames(List<String> serviceNames){
		this.serviceNames = serviceNames;
	}
	
	/**
	 * @see Object#toString()
	 */
	public String toString(){
		StringBuilder sb = new StringBuilder("spaceName:");
		sb.append(spaceName).append(",").append(super.toString());
		sb.append(",serviceNames:").append(serviceNames);
		return sb.toString();
	}
}
