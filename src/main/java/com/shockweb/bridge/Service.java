package com.shockweb.bridge;

import java.util.List;

/**
 * 服务定义
 * 
 * @author 彭明华
 * 2017年12月12日 创建
 */
public class Service {
	


	/**
	 * 注册的服务名
	 */
	private String serviceName = null;
	
	/**
	 * 注册的服务名
	 * @return
	 */
	public String getServiceName(){
		return serviceName;
	}
	/**
	 * 设置注册的服务名
	 * 
	 * @param serviceName
	 */
	public void setServiceName(String serviceName){
		this.serviceName = serviceName;
	}
	
	/**
	 * 对应的服务地址
	 */
	private List<String> serviceHosts = null;
	/**
	 * 对应的服务地址
	 * @return
	 */
	public List<String> getServiceHosts(){
		return serviceHosts;
	}
	/**
	 * 设置对应的服务地址
	 * @param serviceHosts
	 */
	public void setServiceHosts(List<String> serviceHosts){
		this.serviceHosts = serviceHosts;
	}
	
	/**
	 * @see Object#toString()
	 */
	public String toString(){
		StringBuilder sb = new StringBuilder("serviceName:");
		sb.append(serviceName).append(",serviceHosts:").append(serviceHosts);
		return sb.toString();
	}
}
