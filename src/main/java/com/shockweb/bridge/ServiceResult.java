package com.shockweb.bridge;

import com.shockweb.common.context.ContextParam;
/**
 * 请求返回的结果
 * 
 * @author 彭明华
 * 2018年1月10日 创建
 */
public class ServiceResult {
	/**
	 * 上下文参数
	 */
	private ContextParam context = null;
	
	/**
	 * 设置上下文参数
	 * @param pararms
	 */
	public void setContextParam(ContextParam context){
		this.context = context;
	}
	
	/**
	 * 上下文参数
	 */
	public ContextParam getContextParam(){
		return context;
	}
	
	/**
	 * 服务的参数
	 */
	private Object result = null;
	
	/**
	 * 设置服务的参数
	 * @param pararms
	 */
	public void setResult(Object result){
		this.result = result;
	}
	
	/**
	 * 服务的参数
	 */
	public Object getResult(){
		return result;
	}
	
	/**
	 * @see Object#toString()
	 */
	public String toString(){
		return "context:" + context + ",result:" + result; 
	}
}
