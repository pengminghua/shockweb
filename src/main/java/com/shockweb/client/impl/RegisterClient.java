package com.shockweb.client.impl;

import java.util.List;
import java.util.Map;

import com.shockweb.bridge.HostInfo;
import com.shockweb.bridge.OperationDefine;
import com.shockweb.bridge.ServerInfo;
import com.shockweb.bridge.ServiceSpace;
import com.shockweb.client.Client;
import com.shockweb.client.exception.ClientException;
import com.shockweb.service.exception.ServerException;

/**
 * 微服务连接到注册服务器客户端
 * 
 * @author 彭明华
 * 2018年1月2日 创建
 */
public class RegisterClient extends Client{

	/**
	 * @see Client#Client()
	 */
	public RegisterClient(){
		super();
	}
	
    /**
     * @see Client#Client(int, int, int, int)
     * @param timeOut
     * @param connectTimeOut
     * @param sleepTime 
     * @param idleStateTime
     */
    public RegisterClient(int timeOut,int connectTimeOut,int sleepTime,int idleStateTime){
    	super(timeOut,connectTimeOut,sleepTime,idleStateTime);
    }
    
    /**
     * 查询注册服务器所有服务
     * @return 返回所有服务定义
     * @throws ClientException
     */
    @SuppressWarnings("unchecked")
	public Map<String,ServiceSpace> queryServices() throws ClientException {
   		return (Map<String,ServiceSpace>)send(OperationDefine.REQ_QUERY_SERVICES,null,Map.class);
    }
 

    /**
     * 将本地需要同步的服务同步到其他注册中心
     * @param services
     * @throws ServerException
     */
    public void syncServices(List<ServerInfo> services) throws ClientException {
    	if(services!=null && !services.isEmpty()){
    		send(OperationDefine.REQ_SYNC_SERVICES, services,null);
    	}
    }
 
    /**
     * 将本地服务器状态同步到微服务中心
     * @param hosts
     * @return
     * @throws ServerException
     */
    public void syncHosts(List<HostInfo> hosts) throws ClientException {
    	if(hosts!=null && !hosts.isEmpty()){
    		send(OperationDefine.REQ_SYNC_HOSTS, hosts,null);
    	}
    }
    

    /**
     * 将本地服务发布到注册中心
     * @param info
     * @return 
     * @throws ServerException
     */
    public void putServices(ServerInfo info) throws ClientException {
    	if(info!=null){
    		send(OperationDefine.REQ_PUT_SERVICES, info,null);
    	}
    }
 
    /**
     * 将本地服务器状态发布到注册中心
     * @param info
     * @return
     * @throws ServerException
     */
    public void putHosts(HostInfo info) throws ClientException {
    	if(info!=null){
    		send(OperationDefine.REQ_PUT_HOSTS, info,null);
    	}
    }
}
