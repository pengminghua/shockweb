package com.shockweb.client;

/**
 * 客户端请求结构体
 * 
 * @author 彭明华
 * 2017年12月27日 创建
 */
public class Request{
	
	/**
	 * 返回数据
	 */
	public byte[] data = null;
	
	/**
	 * 请求锁
	 */
	public Object Lock = new Object();
	/**
	 * 返回异常
	 */
	public Throwable exception = null;
	/**
	 * 是否返回标志
	 */
	public boolean returnValue = false;

}
