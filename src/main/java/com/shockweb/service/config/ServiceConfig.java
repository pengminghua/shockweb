package com.shockweb.service.config;

/**
 * 微服务服务器配置
 * 
 * @author 彭明华
 * 2018年1月3日 创建
 */
public class ServiceConfig {

	/**
	 * 配置中心地址
	 */
	private String configCenterUrls = null;
	
	/**
	 * 设置配置中心地址
	 * @param configCenterUrls
	 */
	public void setConfigCenterUrls(String configCenterUrls){
		this.configCenterUrls = configCenterUrls;
	}
	
	/**
	 * 配置中心地址
	 * @return
	 */
	public String getConfigCenterUrls(){
		return configCenterUrls;
	}
	
	/**
	 * 配置中心group
	 */
	private String configCenterGroup = null;
	
	/**
	 * 设置配置中心group
	 * @param configCenterGroup
	 */
	public void setConfigCenterGroup(String configCenterGroup){
		this.configCenterGroup = configCenterGroup;
	}
	
	/**
	 * 配置中心group
	 * @return
	 */
	public String getConfigCenterGroup(){
		return configCenterGroup;
	}

	/**
	 * 当前微服务注册到注册中心的命名空间
	 */
	private String spaceName = "default";
	
	/**
	 * 设置当前微服务注册到注册中心的命名空间
	 * @param serviceSpace
	 */
	public void setSpaceName(String spaceName){
		this.spaceName = spaceName;
	}
	
	/**
	 * 当前微服务注册到注册中心的命名空间
	 * @return
	 */
	public String getSpaceName(){
		return spaceName;
	}
	
	
	/**
	 * 当前服务器的url
	 */
	private String hostUrl = null;
	
	/**
	 * 设置当前服务器的url
	 * @param hostUrl
	 */
	public void setHostUrl(String hostUrl){
		this.hostUrl = hostUrl;
	}
	
	/**
	 * 当前服务器的url
	 * @return
	 */
	public String getHostUrl(){
		return hostUrl;
	}
	
	/**
	 * 所有注册服务器集群的url
	 */
	private String registerServerUrls = null;
	
	/**
	 * 设置所有注册服务器集群的url
	 * @param hostUrl
	 */
	public void setRegisterServerUrls(String registerServerUrls){
		this.registerServerUrls = registerServerUrls;
	}
	
	/**
	 * 所有注册服务器集群的url
	 * @return
	 */
	public String getRegisterServerUrls(){
		return registerServerUrls;
	}

	/**
	 * 同步线程的每次循环延迟等待时间ms
	 */
	public int syncThreadSleepTime = 1000;
	/**
	 * 设置同步线程的每次循环延迟等待时间ms
	 * @param syncThreadSleepTime
	 */
	public void setSyncThreadSleepTime(int syncThreadSleepTime){
		this.syncThreadSleepTime = syncThreadSleepTime;
	}
	
	/**
	 * 同步线程的每次循环延迟等待时间ms
	 * @return
	 */
	public int getSyncThreadSleepTime(){
		return syncThreadSleepTime;
	}
	
	/**
	 * 同步微服务限流线程的每次循环延迟等待时间ms
	 */
	public int serviceThresholdSleepTime = 10000;
	/**
	 * 设置同步微服务限流线程的每次循环延迟等待时间ms
	 * @param serviceThresholdSleepTime
	 */
	public void setServiceThresholdSleepTime(int serviceThresholdSleepTime){
		this.serviceThresholdSleepTime = serviceThresholdSleepTime;
	}
	
	/**
	 * 同步微服务限流线程的每次循环延迟等待时间ms
	 * @return
	 */
	public int getServiceThresholdSleepTime(){
		return serviceThresholdSleepTime;
	}
	
	/**
	 * 发送到注册服务器服务及服务器信息周期
	 */
	public int putServicesCycle = 30;
	/**
	 * 设置发送到注册服务器服务及服务器信息周期
	 * @param putServicesCycle
	 */
	public void setPutServicesCycle(int putServicesCycle){
		this.putServicesCycle = putServicesCycle;
	}
	
	/**
	 * 发送到注册服务器服务及服务器信息周期
	 * @return
	 */
	public int getPutServicesCycle(){
		return putServicesCycle;
	}
	
	/**
	 * 发送到注册服务器服务器信息周期
	 */
	public int putHostsCycle = 3;
	/**
	 * 设置发送到注册服务器服务器信息周期
	 * @param putHostsCycle
	 */
	public void setPutHostsCycle(int putHostsCycle){
		this.putHostsCycle = putHostsCycle;
	}
	
	/**
	 * 发送到注册服务器服务器信息周期
	 * @return
	 */
	public int getPutHostsCycle(){
		return putHostsCycle;
	}
	
	/**
	 * 检测心跳检测时间
	 */
	private int serverIdleStateTime = 1000*12;
	
	/**
	 * 设置检测心跳检测时间
	 * @param serverIdleStateTime
	 */
	public void setServerIdleStateTime(int serverIdleStateTime){
		this.serverIdleStateTime = serverIdleStateTime;
	}
	
	/**
	 * 检测心跳检测时间
	 * @return
	 */
	public int getServerIdleStateTime() {
		return serverIdleStateTime;
	}
	
	/**
	 * 客户端建立连接的超时时间
	 */
	private int clientConnectTimeOut = 5000;
	
	/**
	 * 设置客户端建立连接的超时时间
	 * @param clientConnectTimeOut
	 */
	public void setClientConnectTimeOut(int clientConnectTimeOut){
		this.clientConnectTimeOut = clientConnectTimeOut;
	}
	
	/**
	 * 客户端建立连接的超时时间
	 * @return
	 */
	public int getClientConnectTimeOut() {
		return clientConnectTimeOut;
	}
	
	/**
	 * 客户端请求的超时时间
	 */
    private int clientTimeOut = 5000;
    
	/**
	 * 设置客户端请求的超时时间
	 * @param clientTimeOut
	 */
	public void setClientTimeOut(int clientTimeOut){
		this.clientTimeOut = clientTimeOut;
	}
	
	/**
	 * 客户端请求的超时时间
	 * @return
	 */
	public int getClientTimeOut() {
		return clientTimeOut;
	}
	

	/**
	 * 客户端等待时扫描时间结果
	 */
    private int clientSleepTime = 20;
    
	/**
	 * 设置客户端等待时扫描时间结果
	 * @param clientSleepTime
	 */
	public void setClientSleepTime(int clientSleepTime){
		this.clientSleepTime = clientSleepTime;
	}
	
	/**
	 * 客户端等待时扫描时间结果
	 * @return
	 */
	public int getClientSleepTime() {
		return clientSleepTime;
	}
	
	

	
	/**
	 * 长连接心跳发送时间
	 */
	private int clientIdleStateTime = 1000*10;
	
	/**
	 * 检测长连接心跳发送时间
	 * @param clientIdleStateTime
	 */
	public void setClientIdleStateTime(int clientIdleStateTime){
		this.clientIdleStateTime = clientIdleStateTime;
	}
	
	/**
	 * 长连接心跳发送时间
	 * @return
	 */
	public int getClientIdleStateTime() {
		return clientIdleStateTime;
	}

	/**
	 * 初始化类
	 */
	private String initializations = null;
	
	/**
	 * 初始化类
	 * @return
	 */
	public String getInitializations(){
		return initializations;
	}
	
	/**
	 * 设置初始化类
	 * @param initializations
	 */
	public void setInitializations(String initializations){
		this.initializations = initializations;
	}
	
	
	/**
	 * 正在处理的服务最大阈值
	 */
	private long doingThreshold = 0;
	
	/**
	 * 正在处理的服务最大阈值
	 * @return
	 */
	public long getDoingThreshold() {
		return doingThreshold;
	}
	
	/**
	 * 设置正在处理的服务最大阈值
	 * @param doingThreshold
	 */
	public void setDoingThreshold(long doingThreshold){
		this.doingThreshold = doingThreshold;
	}
	
	

	/**
	 * 客户端请求的超时时间
	 */
    private int serviceTimeOut = 5000;
    
	/**
	 * 设置客户端请求的超时时间
	 * @param clientTimeOut
	 */
	public void setServiceTimeOut(int serviceTimeOut){
		this.serviceTimeOut = serviceTimeOut;
	}
	
	/**
	 * 客户端请求的超时时间
	 * @return
	 */
	public int getServiceTimeOut() {
		return serviceTimeOut;
	}
	
	/**
	 * ioc配置文件的路径
	 */
    private String path = null;
    
	/**
	 * 设置ioc配置文件的路径
	 * @param path
	 */
	public void setPath(String path){
		this.path = path;
	}
	
	/**
	 * ioc配置文件的路径
	 * @return
	 */
	public String getPath() {
		return path;
	}
}
