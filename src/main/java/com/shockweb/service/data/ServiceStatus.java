package com.shockweb.service.data;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 服务定义及状态
 * 
 * @author 彭明华
 * 2018年1月3日 创建
 */
public class ServiceStatus{
	
	/**
	 * 隐藏构造方法
	 */
	private ServiceStatus(){
		
	}

	/**
	 * 当前的服务
	 */
	private static ServiceStatus services = new ServiceStatus();
	
	/**
	 * 服务定义及状态
	 * @return
	 */
	public static ServiceStatus getServices(){
		return services;
	}
	
	/**
	 * 服务空间名
	 */
	private String spaceName = null;
	
	/**
	 * 服务空间名
	 * @return
	 */
	public String getSpaceName(){
		return spaceName;
	}
	
	/**
	 * 设置服务空间名
	 * @param spaceName
	 */
	public void setSpaceName(String spaceName){
		this.spaceName = spaceName;
	}
	
	/**
	 * 服务空间
	 */
	private Map<String,Long> serviceDefines = new HashMap<String,Long>();
	
    private final ReadWriteLock serviceLock = new ReentrantReadWriteLock();
	
	/**
	 * 返回服务及状态定义
	 * @return
	 */
	public Map<String,Long> getServiceDefines(){
		return serviceDefines;
	}
	/**
	 * 将服务添加到spaceName下
	 * @param serviceName
	 */
	public void add(String serviceName){
		try {
			serviceLock.writeLock().lock();
			if(!serviceDefines.containsKey(serviceName)){
				serviceDefines.put(serviceName, 0L);
			}
		}finally {
			serviceLock.writeLock().unlock();
		}
	}

	/** 
	 * 	清除所有服务
	 * @param spaceName
	 */
	public void clear(){
		try {
			serviceLock.writeLock().lock();
			serviceDefines.clear();
		}finally {
			serviceLock.writeLock().unlock();
		}
	}
	
	/**
	 * 被调用次数
	 * @param serviceName
	 * @return
	 */
	public long getCalled(String serviceName){
		try {
			serviceLock.readLock().lock();
			if(serviceDefines.containsKey(serviceName)){
				return serviceDefines.get(serviceName);
			}
		}finally {
			serviceLock.readLock().unlock();
		}
		return 0;
	}
	
	/**
	 * 设置被调用次数
	 * @param serviceName
	 */
	public void addCalled(String serviceName){
		try {
			serviceLock.writeLock().lock();
			if(serviceDefines.containsKey(serviceName)){
				serviceDefines.put(serviceName,serviceDefines.get(serviceName)+1);
			}
		}finally {
			serviceLock.writeLock().unlock();
		}
	}
	
	/**
	 * @see Object#toString()
	 */
	public String toString(){
		return "spaceName=" + spaceName + ",serviceDefines=" + serviceDefines;
	}
}
