package com.shockweb.service.init;

import com.shockweb.service.config.ServiceConfig;
import com.shockweb.service.exception.ServerException;

/**
 * 初始化接口
 * 
 * @author 彭明华
 * 2018年1月19日 创建
 */
public interface Initialization {
	/**
	 * 初始化方法
	 * @param path 配置文件路径
	 * @param config 配置文件
	 * @throws ServerException
	 */
	public void init(String path,ServiceConfig config)throws ServerException;
	
}
