package com.shockweb.service.init.impl;


import com.shockweb.service.config.ServiceConfig;
import com.shockweb.service.exception.ServerException;
import com.shockweb.service.init.Initialization;
import com.shockweb.service.ioc.IocManager;
import com.shockweb.service.ioc.impl.SpringWebIocInvokeImpl;

/**
 * Spring web初始化类
 * 
 * @author 彭明华
 * 2018年2月22日 创建
 */
public class SpringWebInitImpl implements Initialization{

	
	/**
	 * 初始化方法
	 */
	public void init(String path,ServiceConfig config)throws ServerException{
		IocManager.add(new SpringWebIocInvokeImpl());
	}
}
