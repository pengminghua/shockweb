package com.shockweb.service;

import com.shockweb.service.exception.ServerException;

/**
 * 
 * 
 * @author 彭明华
 * 2018年2月22日 创建
 */
public class SpringWebStart {
	
	/**
	 * 初始化方法
	 * @throws ServerException
	 */
	public void initMethod() throws ServerException{
		ServiceServer.start(filePath);
	}
	
	/**
	 * 停止服务
	 * @throws ServerException
	 */
	public void destroyMethod() throws ServerException{
		ServiceServer.stop();
	}
	
	
	/**
	 * prop文件及路径
	 */
	public String filePath;
	
	/**
	 * 设置prop文件及路径
	 * @param filePath
	 */
	public void setFilePath(String filePath){
		this.filePath = filePath;
	}
}
