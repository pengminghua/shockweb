package com.shockweb.service;

import com.shockweb.service.config.ServiceConfig;
import com.shockweb.service.exception.ServerException;
import com.shockweb.service.init.Initialization;

/**
 * 初始化类管理器
 * 
 * @author 彭明华
 * 2018年1月17日 创建
 */
public class InitManager {

	/**
	 * 执行所有初始化类
	 * @param path
	 * @param config
	 * @throws ServerException
	 */
	public static void init(ServiceConfig config)throws ServerException{
		
		if(config.getInitializations()!=null){
			String[] initDefines = config.getInitializations().split(",");
			for(String define:initDefines){
				try{
					Object o = Class.forName(define).newInstance();
					if(o instanceof Initialization){
						Initialization init = (Initialization)o;
						init.init(config.getPath(), config);
					}
				}catch(ServerException e){
					throw e;
				}catch(Exception e){
					throw new ServerException("初始化实例化" + define + "类出错");
				}
			}
		}

	}
	
}
