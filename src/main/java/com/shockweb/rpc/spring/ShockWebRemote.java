package com.shockweb.rpc.spring;

import java.lang.annotation.Target;

import org.springframework.stereotype.Service;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Documented;


/**
 * 客户端本地服务接口
 * 
 * @author 彭明华
 * 2018年1月25日 创建
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Service
public @interface ShockWebRemote{

	/**
	 * 注册服务中心
	 * @return
	 */
	String registerCenter() default "";
	
	/**
	 * 微服务名称
	 */
	String value() default "";

	/**
	 * 微服务命名空间
	 */
	String spaceName() default "";

	/**
	 * 是否同步上下文
	 * @return
	 */
	boolean context() default true;

	/**
	 * 是否广播到所有能连上的服务器执行
	 * @return
	 */
	boolean broadcast() default false;
	
	/**
	 * 是否异步执行
	 * @return
	 */
	boolean asynchronous() default false;
}
