package com.shockweb.rpc.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import com.shockweb.bridge.Service;
import com.shockweb.bridge.ServiceHost;
import com.shockweb.bridge.ServiceSpace;

/**
 * 注册服务器所有服务的缓存数据
 * 
 * @author 彭明华
 * 2018年1月3日 创建
 */
public class ServiceRoot{
	
	/**
	 * 构造方法
	 */
	public ServiceRoot(){}
	
	
	/**
	 * 存储所有的服务
	 */
	private Map<String,ServiceSpace> serviceSpaces = new HashMap<String,ServiceSpace>();
	
    /**
     * 当前类的读写锁
     */
    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    
    /**
     * 获取读写锁
     * @return
     */
    public ReadWriteLock getLock(){
    	return lock;
    }
	/**
	 * 获取服务空间
	 * @param spaceName
	 * @return
	 */
	public void putServiceSpace(Map<String,ServiceSpace> serviceSpaces){
		try{
			lock.writeLock().lock();
			this.serviceSpaces = serviceSpaces;
		}finally{
			lock.writeLock().unlock();
		}
	}

	
	/**
	 * 返回可用的服务
	 * @param spaceName
	 * @param serviceName
	 * @return
	 */
	public List<String> getHosts(String spaceName,String serviceName){
		Service service = null;
		try{
			lock.readLock().lock();
			if(serviceSpaces!=null){
				ServiceSpace serviceSpace = serviceSpaces.get(spaceName);
				if(serviceSpace!=null){
					service = serviceSpace.getService(serviceName);
				}
			}
		}finally{
			lock.readLock().unlock();
		}
		if(service!=null){
			synchronized(service){
				List<String> lists = service.getServiceHosts();
				return lists;
			}
		}
		return null;
	}
	
	
	/**
	 * 返回服务的定义
	 * @return
	 */
	public Map<String,ServiceSpace> getServiceSpaces(){
		try{
			lock.readLock().lock();
			Map<String,ServiceSpace> rtn = new HashMap<String,ServiceSpace>();
			Iterator<Entry<String,ServiceSpace>> its = serviceSpaces.entrySet().iterator();
			while(its.hasNext()){
				Entry<String,ServiceSpace> entry = its.next();
				ServiceSpace serviceSpace = new ServiceSpace();
				serviceSpace.setName(entry.getValue().getName());
				Iterator<Entry<String,Service>> serviceIts = entry.getValue().getServices().entrySet().iterator();
				while(serviceIts.hasNext()){
					Entry<String,Service> serviceEntry = serviceIts.next();
					Service service = new Service();
					service.setServiceName(serviceEntry.getValue().getServiceName());
					List<String> serviceHosts = new ArrayList<String>();
					for(String host:serviceEntry.getValue().getServiceHosts()){
						serviceHosts.add(host);
					}
					service.setServiceHosts(serviceHosts);
					serviceSpace.putService(service.getServiceName(), service);
				}
				Iterator<Entry<String,ServiceHost>> hostIts = entry.getValue().getServiceHosts().entrySet().iterator();
				while(hostIts.hasNext()){
					Entry<String,ServiceHost> hostEntry = hostIts.next();
					ServiceHost serviceHost = new ServiceHost();
					serviceHost.setHost(hostEntry.getValue().getHost());
					serviceHost.setCalled(hostEntry.getValue().getCalled());
					serviceSpace.putServiceHost(serviceHost.getHost(), serviceHost);
				}
				rtn.put(serviceSpace.getName(), serviceSpace);
			}
			return rtn;
		}finally{
			lock.readLock().unlock();
		}
	}
    
    

    
	/**
	 * @see Object#toString()
	 */
	public String toString(){
		StringBuilder sb = new StringBuilder("serviceSpaces:");
		sb.append(serviceSpaces);
		return sb.toString();
	}
}
