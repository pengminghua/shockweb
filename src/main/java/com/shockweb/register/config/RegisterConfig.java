package com.shockweb.register.config;

/**
 * 注册服务器配置信息
 * 
 * @author 彭明华
 * 2018年1月3日 创建
 */
public class RegisterConfig {
	
	/**
	 * 配置中心地址
	 */
	private String configCenterUrls = null;
	
	/**
	 * 设置配置中心地址
	 * @param configCenterUrls
	 */
	public void setConfigCenterUrls(String configCenterUrls){
		this.configCenterUrls = configCenterUrls;
	}
	
	/**
	 * 配置中心地址
	 * @return
	 */
	public String getConfigCenterUrls(){
		return configCenterUrls;
	}
	
	/**
	 * 配置中心group
	 */
	private String configCenterGroup = null;
	
	/**
	 * 设置配置中心group
	 * @param configCenterGroup
	 */
	public void setConfigCenterGroup(String configCenterGroup){
		this.configCenterGroup = configCenterGroup;
	}
	
	/**
	 * 配置中心group
	 * @return
	 */
	public String getConfigCenterGroup(){
		return configCenterGroup;
	}
	
	
	/**
	 * 当前注册服务器的url
	 */
	private String hostUrl = null;
	
	/**
	 * 设置当前注册服务器的url
	 * @param hostUrl
	 */
	public void setHostUrl(String hostUrl){
		this.hostUrl = hostUrl;
	}
	
	/**
	 * 当前注册服务器的url
	 * @return
	 */
	public String getHostUrl(){
		return hostUrl;
	}
	
	/**
	 * 所有注册服务器集群的url，逗号分隔
	 */
	private String registerServerUrls = null;
	
	/**
	 * 设置所有注册服务器集群的url，逗号分隔
	 * @param hostUrl
	 */
	public void setRegisterServerUrls(String registerServerUrls){
		this.registerServerUrls = registerServerUrls;
	}
	
	/**
	 * 所有注册服务器集群的url，逗号分隔
	 * @return
	 */
	public String getRegisterServerUrls(){
		return registerServerUrls;
	}


    /**
     * 服务注册服务的有效时间
     */
    private int activeTime = 5000;
    
    /**
     * 设置服务注册服务的有效时间
     * @param activeTime
     */
    public void setActiveTime(int activeTime){
    	this.activeTime = activeTime;
    }
    
	/**
	 * 服务注册服务的有效时间
	 * @return
	 */
	public int getActiveTime(){
		return activeTime;
	}
	
	/**
	 * 同步线程的每次循环延迟等待时间
	 */
	public int syncThreadSleepTime = 1000;
	
	/**
	 * 设置同步线程的每次循环延迟等待时间
	 * @param syncThreadSleepTime
	 */
	public void setSyncThreadSleepTime(int syncThreadSleepTime){
		this.syncThreadSleepTime = syncThreadSleepTime;
	}
	
	/**
	 * 同步线程的每次循环延迟等待时间
	 * @return
	 */
	public int getSyncThreadSleepTime(){
		return syncThreadSleepTime;
	}
	
	/**
	 * 服务端每次检测netty客户端心跳间隔时间
	 */
	private int serverIdleStateTime = 1000*12;
	
	/**
	 * 设置服务端每次检测netty客户端心跳间隔时间
	 * @param serverIdleStateTime
	 */
	public void setServerIdleStateTime(int serverIdleStateTime){
		this.serverIdleStateTime = serverIdleStateTime;
	}
	
	/**
	 * 服务端每次检测netty客户端心跳间隔时间
	 * @return
	 */
	public int getServerIdleStateTime() {
		return serverIdleStateTime;
	}
	
	/**
	 * 链接到其他注册中心客户端建立连接的超时时间
	 */
	private int clientConnectTimeOut = 5000;
	
	/**
	 * 设置链接到其他注册中心客户端建立连接的超时时间
	 * @param clientConnectTimeOut
	 */
	public void setClientConnectTimeOut(int clientConnectTimeOut){
		this.clientConnectTimeOut = clientConnectTimeOut;
	}
	
	/**
	 * 链接到其他注册中心客户端建立连接的超时时间
	 * @return
	 */
	public int getClientConnectTimeOut() {
		return clientConnectTimeOut;
	}
	
	/**
	 * 发送到其他注册中心请求的客户端请求的超时时间
	 */
    private int clientTimeOut = 5000;
    
	/**
	 * 设置发送到其他注册中心请求的客户端请求的超时时间
	 * @param clientTimeOut
	 */
	public void setClientTimeOut(int clientTimeOut){
		this.clientTimeOut = clientTimeOut;
	}
	
	/**
	 * 发送到其他注册中心请求的客户端请求的超时时间
	 * @return
	 */
	public int getClientTimeOut() {
		return clientTimeOut;
	}

	/**
	 * 客户端等待时扫描时间结果
	 */
    private int clientSleepTime = 20;
    
	/**
	 * 设置客户端等待时扫描时间结果
	 * @param clientSleepTime
	 */
	public void setClientSleepTime(int clientSleepTime){
		this.clientSleepTime = clientSleepTime;
	}
	
	/**
	 * 客户端等待时扫描时间结果
	 * @return
	 */
	public int getClientSleepTime() {
		return clientSleepTime;
	}
	
	/**
	 * netty客户端长连向服务端发送心跳时间，当前注册中心客户端时间间隔必需小于其他注册中心serverIdleStateTime
	 */
	private int clientIdleStateTime = 1000*10;
	
	/**
	 * 设置netty客户端长连向服务端发送心跳时间，当前注册中心客户端时间间隔必需小于其他注册中心serverIdleStateTime
	 * @param clientIdleStateTime
	 */
	public void setClientIdleStateTime(int clientIdleStateTime){
		this.clientIdleStateTime = clientIdleStateTime;
	}
	
	/**
	 * netty客户端长连向服务端发送心跳时间，当前注册中心客户端时间间隔必需小于其他注册中心serverIdleStateTime
	 * @return
	 */
	public int getClientIdleStateTime() {
		return clientIdleStateTime;
	}
}
