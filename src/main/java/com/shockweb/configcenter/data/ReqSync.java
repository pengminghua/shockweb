package com.shockweb.configcenter.data;

import java.util.Map;

/**
 * 主配置中心刚启动初始化
 * 
 * @author 彭明华
 * 2018年3月20日 创建
 */
public class ReqSync implements IReq{
	
    
	/**
	 * 签名值
	 */
	private String sign = null;
	
	/**
	 * 配置签名值
	 * @param sign
	 */
	public void setSign(String sign){
		this.sign = sign;
	}
	
	/**
	 * 签名值
	 * @return
	 */
	public String getSign(){
		return sign;
	}
	
	/**
	 * 同步数据
	 */
	private Map<String,Map<String,String>> configs = null;
	
	/**
	 * 配置同步数据
	 * @param data
	 */
	public void setConfigs(Map<String,Map<String,String>> configs){
		this.configs = configs;
	}
	
	/**
	 * 同步数据
	 * @return
	 */
	public Map<String,Map<String,String>> getConfigs(){
		return configs;
	}
	
	
	/**
	 * 时间
	 */
	private long time = System.currentTimeMillis();
	
	/**
	 * 配置时间
	 * @param name
	 */
	public void setTime(long time){
		this.time = time;
	}
	
	/**
	 * 时间
	 * @return
	 */
	public long getTime(){
		return time;
	}
    
	/**
	 * @see Object#toString()
	 */
	public String toString(){
		StringBuilder sb = new StringBuilder("configs:");
		sb.append(configs);
		sb.append("time:");
		sb.append(time);
		return sb.toString();
	}
	
}
