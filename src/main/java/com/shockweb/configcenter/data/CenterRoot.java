package com.shockweb.configcenter.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 配置中心的所有配置保存的地方
 * 
 * @author 彭明华
 * 2018年3月20日 创建
 */
public class CenterRoot{
	
	/**
	 * 配置中心根服务
	 */
	private static CenterRoot centerRoot = new CenterRoot();
	
	/**
	 * 获取配置中心根服务
	 * @return
	 */
	public static CenterRoot getCenterRoot(){
		synchronized(centerRoot){
			return centerRoot;
		}
	}


	/**
	 * 配置中心的所有数据
	 */
	private Map<String,Map<String,String>> configs = new HashMap<String,Map<String,String>>();
	
	/**
	 * 初始化配置中心数据
	 * @param configs
	 */
	public void syncConfig(Map<String,Map<String,String>> configs){
		synchronized(configs){
			this.configs.clear();
			if(configs!=null && !configs.isEmpty()){
				this.configs.putAll(configs);
			}
		}
	}
	
	/**
	 * 获取所有的配置
	 * @return
	 */
	public Map<String,Map<String,String>> getConfigs(){
		return configs;
	}
	
	/**
	 * 获取配置
	 * @param group
	 * @param name
	 * @return
	 */
	public String getConfig(String group,String name){
		Map<String,String> map = configs.get(group);
		if(map!=null){
			return map.get(name);
		}
		return null;
	}

	
	/**
	 * 设置配置
	 * @param group
	 * @param name
	 * @param config
	 */
	public void putConfig(String group,String name,String value){
		if(group!=null && name!=null && value!=null){
			synchronized(configs){
				Map<String,String> map = configs.get(group);
				if(map==null){
					map = new HashMap<String,String>();
					configs.put(group, map);
				}
				map.put(name,value);
			}
		}
	}
	
	/**
	 * 删除配置
	 * @param group
	 * @param name
	 */
	public void removeConfig(String group,String name){
		if(group!=null && name!=null){
			synchronized(configs){
				Map<String,String> map = configs.get(group);
				if(map!=null){
					map.remove(name);
				}
			}
		}
	}

	/**
	 * 清空所有配置
	 */
	public void clear(){
		configs.clear();;
	}
	
	/**
	 * 获取所有Name
	 * @param group
	 * @return
	 */
	public List<String> getNames(String group){
		if(group!=null){
			synchronized(configs){
				if(configs.get(group)!=null){
					List<String> list = new ArrayList<String>();
					list.addAll(configs.get(group).keySet());
					return list;
				}else{
					return null;
				}
			}
		}else{
			return null;
		}
	}
	
	/**
	 * 获取所有Name
	 * @return
	 */
	public List<String> getGroups(){
		List<String> list = new ArrayList<String>();
		list.addAll(configs.keySet());
		return list;
	}
    
	/**
	 * @see Object#toString()
	 */
	public String toString(){
		StringBuilder sb = new StringBuilder("configs:");
		sb.append(configs);
		return sb.toString();
	}
	
}
