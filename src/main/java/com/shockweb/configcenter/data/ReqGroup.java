package com.shockweb.configcenter.data;



/**
 * 带group的请求
 * 
 * @author 彭明华
 * 2018年3月20日 创建
 */
public class ReqGroup implements IReq{
	
	/**
	 * 分组
	 */
	private String group = null;
	
	/**
	 * 配置分组
	 * @param group
	 */
	public void setGroup(String group){
		this.group = group;
	}
	
	/**
	 * 分组
	 * @return
	 */
	public String getGroup(){
		return group;
	}
	
	/**
	 * 签名值
	 */
	private String sign = null;
	
	/**
	 * 配置签名值
	 * @param sign
	 */
	public void setSign(String sign){
		this.sign = sign;
	}
	
	/**
	 * 签名值
	 * @return
	 */
	public String getSign(){
		return sign;
	}
	
	/**
	 * 时间
	 */
	private long time = System.currentTimeMillis();
	
	/**
	 * 配置时间
	 * @param name
	 */
	public void setTime(long time){
		this.time = time;
	}
	
	/**
	 * 时间
	 * @return
	 */
	public long getTime(){
		return time;
	}
    
	/**
	 * @see Object#toString()
	 */
	public String toString(){
		StringBuilder sb = new StringBuilder("group:");
		sb.append(group);
		sb.append(",time:");
		sb.append(time);
		return sb.toString();
	}
	
}
